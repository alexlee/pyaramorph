# pyaramorph, an Arabic morphological analyzer
# Copyright © 2005 Alexander Lee
# Ported to Python from Tim Buckwalter's aramorph.pl.

import sys
import re
from collections import namedtuple
from . import buckwalter
from . import util

Segment = namedtuple('Segment', 'prefix stem suffix')

# Data file paths
TABLE_AB = "tableAB"
TABLE_BC = "tableBC"
TABLE_AC = "tableAC"
DICT_PREFIXES = "dictPrefixes"
DICT_STEMS = "dictStems"
DICT_SUFFIXES = "dictSuffixes"

RE_ARABIC = re.compile('[\u0621-\u0652\u0670-\u0671]+')
"""Include all strictly Arabic consonants and diacritics -- perhaps
include other letters at a later time."""

def _tokenize(text):
    """Extract all Arabic words from input and ignore everything
    else."""
    return (match.group() for match in RE_ARABIC.finditer(text))

def _clean_arabic(text):
    """Remove any تَطوِيل and vowels/diacritics."""
    return re.sub('[\u0640\u064b-\u0652\u0670]', '', text)
    # FIXME do something about \u0671, ALIF WASLA ?

class ParseItem:
    """Stores parsing info for a single Arabic word."""

    def __init__(self, univoc, buckvoc, lemmaID, pos, gloss):
        self.univoc = univoc
        self.buckvoc = buckvoc
        self.lemmaID = lemmaID
        self.pos = pos
        self.gloss = gloss

    def _pos_str(self):
        return "".join(self.pos)

    def _gloss_str(self):
        return " + ".join(self.gloss)

    def __str__(self):
        return (
            "    solution: ({s.univoc} {s.buckvoc}) [{s.lemmaID}]\n"
            "         pos: {pos}\n"
            "       gloss: {gloss}"
        ).format(s=self, pos=self._pos_str(), gloss=self._gloss_str())

class WordAnalysis:
    """Stores the possible parses of a given Arabic word."""

    def __init__(self, word, parses):
        self.word = word
        self.buck = buckwalter.uni2buck(word)
        self.parses = parses

    def __len__(self):
        return len(self.parses)

    def __iter__(self):
        for parse in self.parses:
            yield parse

    def __str__(self):
        return "analysis for: {} {}".format(self.word, self.buck)

class Analyzer:
    """Performs analysis of Arabic text, seeing how each word could be
    parsed and interpreted."""

    def __init__(self):
        """Set up reference tables and dictionaries."""
        self.tableAB = util.load_table(TABLE_AB)
        self.tableBC = util.load_table(TABLE_BC)
        self.tableAC = util.load_table(TABLE_AC)

        self.prefixes = util.load_dict(DICT_PREFIXES)
        self.stems = util.load_dict(DICT_STEMS)
        self.suffixes = util.load_dict(DICT_SUFFIXES)

    def analyze_text(self, text):
        """Generate analyses for each word in the given Arabic text."""
        for token in _tokenize(text.strip()):
            token = _clean_arabic(token)
            analysis = self.analyze_word(token)
            if len(analysis) > 0:
                yield analysis

    def analyze_word(self, word):
        """Return all possible ways of parsing the given Arabic word."""
        parses = []
        count = 0
        segments = self._build_segments(word)

        for prefix, stem, suffix in segments:
            parses.extend(self._check_segment(prefix, stem, suffix))

        return WordAnalysis(word, parses)

    def _check_segment(self, prefix, stem, suffix):
        """Return all matches for the given prefix, stem, and suffix."""
        matches = []
        prefixes = self.prefixes[prefix]
        stems = self.stems[stem]
        suffixes = self.suffixes[suffix]

        # Loop through the possible prefix entries
        for pre_entry in prefixes:
            (voc_a, cat_a, gloss_a, pos_a) = pre_entry[1:5]

            # Loop through the possible stem entries
            for stem_entry in stems:
                (voc_b, cat_b, gloss_b, pos_b, lemmaID) = stem_entry[1:]

                # Check the prefix + stem pair
                pairAB = "%s %s" % (cat_a, cat_b)
                if pairAB not in self.tableAB: continue

                # Loop through the possible suffix entries
                for suf_entry in suffixes:
                    (voc_c, cat_c, gloss_c, pos_c) = suf_entry[1:5]

                    # Check the prefix + suffix pair
                    pairAC = "%s %s" % (cat_a, cat_c)
                    if pairAC not in self.tableAC: continue

                    # Check the stem + suffix pair
                    pairBC = "%s %s" % (cat_b, cat_c)
                    if pairBC not in self.tableBC: continue

                    # Ok, it passed!
                    buckvoc = "%s%s%s" % (voc_a, voc_b, voc_c)
                    univoc = buckwalter.buck2uni(buckvoc)
                    if gloss_a == '': gloss_a = '___'
                    if gloss_c == '': gloss_c = '___'

                    # Assemble the result
                    pos = (pos_a, pos_b, pos_c)
                    gloss = (gloss_a, gloss_b, gloss_c)
                    matches.append(ParseItem(univoc, buckvoc, lemmaID,
                        pos, gloss))

        return matches

    def _valid_segment(self, segment):
        """Determine whether the segment is possible."""
        return segment.prefix in self.prefixes \
                and segment.stem in self.stems \
                and segment.suffix in self.suffixes

    def _build_segments(self, word):
        """Return all possible segmentations of the given Arabic
        word."""
        buck = buckwalter.uni2buck(word)
        segments = []

        for stem_idx, suf_idx in util.segment_indexes(len(buck)):
            prefix = buck[0:stem_idx]
            stem = buck[stem_idx:suf_idx]
            suffix = buck[suf_idx:]

            segment = Segment(prefix, stem, suffix)
            if self._valid_segment(segment):
                segments.append(segment)

        return segments

